package com.mindscape.codecraftsample.utility;

/**
 * Created by Akshay on 15-10-2019.
 */

public interface ScrollListener {
    void onLoadMore();
}
