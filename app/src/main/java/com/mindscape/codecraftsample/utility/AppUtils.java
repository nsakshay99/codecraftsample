package com.mindscape.codecraftsample.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.mindscape.codecraftsample.R;

import java.util.ArrayList;
import java.util.List;

public class AppUtils {
    public static final int SUCCESS_RESULT = 0;

    static final int FAILURE_RESULT = 1;

    private static final String PACKAGE_NAME = "com.mindscape.codecraftsample";

    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";

    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";

    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";
    public static final String LOCATION_DATA_EXTRA_DESTINATION = PACKAGE_NAME + ".DESTINATION";

    static final String LOCATION_DATA_AREA = PACKAGE_NAME + ".LOCATION_DATA_AREA";
    static final String LOCATION_DATA_CITY = PACKAGE_NAME + ".LOCATION_DATA_CITY";
    static final String LOCATION_DATA_STREET = PACKAGE_NAME + ".LOCATION_DATA_STREET";
    public static boolean checkPermissionActivity(String[] permissions, int permissionCode, Activity activity) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(activity, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), permissionCode);
            return false;
        }
        return true;
    }

    public static boolean checkPermissionFragment(String[] permissions, int permissionCode, Context context, Fragment fragment) {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String p : permissions) {
            result = ContextCompat.checkSelfPermission(context, p);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(p);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            fragment.requestPermissions(listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), permissionCode);
            return false;
        }
        return true;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public static void showInterntOnlySettingsAlert(final Activity cxt) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                cxt);
        alertDialog.setTitle(cxt.getString(R.string.internet_not_enabled));
        alertDialog.setMessage(cxt.getString(R.string.enable_in_seettingd));
        alertDialog.setPositiveButton(cxt.getString(R.string.enable),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_WIRELESS_SETTINGS);
                        cxt.startActivity(intent);

                    }
                });
        alertDialog.setNegativeButton(cxt.getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        //((Activity) cxt).finish();
                    }
                });
        alertDialog.show();
    }

    public static void showLocationSettingsAlert(final Activity cxt) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                cxt);
        alertDialog.setTitle("Location Required");
        alertDialog.setMessage("Location is required to get data, go to settings to enable it..");
        alertDialog.setPositiveButton("Enable",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        cxt.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        //((Activity) cxt).finish();
                    }
                });
        alertDialog.show();
    }

    public static void showLocationPermissionAlert(final Activity cxt) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                cxt);
        alertDialog.setTitle("Location Required");
        alertDialog.setMessage("Location is required to get data");
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    public static void showFileNotFoundAlert(final Activity cxt) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                cxt);
        alertDialog.setTitle("File Not Found");
        alertDialog.setMessage("Image not found in url");
        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        cxt.finish();
                    }
                });

        alertDialog.show();
    }
}
