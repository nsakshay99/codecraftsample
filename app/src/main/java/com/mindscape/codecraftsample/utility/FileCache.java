package com.mindscape.codecraftsample.utility;

import android.content.Context;

import java.io.File;
import java.io.IOException;

public class FileCache {

    private File cacheDir;

    public FileCache(Context context) {
        //Find the dir to save cached images

        cacheDir = context.getFilesDir();
        if (!cacheDir.exists())
            cacheDir.mkdir();
    }

    public File getFile(String url) {
        String filename = String.valueOf(url.hashCode());
        File f = new File(cacheDir, filename);
        try {
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;

    }

    public void clear() {
        File[] files = cacheDir.listFiles();
        if (files == null)
            return;
        for (File f : files)
            f.delete();
    }

}