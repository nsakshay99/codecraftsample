package com.mindscape.codecraftsample.viewmodel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Looper;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mindscape.codecraftsample.CodeCraftApplication;
import com.mindscape.codecraftsample.R;
import com.mindscape.codecraftsample.ResturantMapsActivity;
import com.mindscape.codecraftsample.adapter.ResturantAdapter;
import com.mindscape.codecraftsample.model.GetResturantData;
import com.mindscape.codecraftsample.model.ResturantResponse;
import com.mindscape.codecraftsample.utility.AppUtils;
import com.mindscape.codecraftsample.utility.ImageLoader;
import com.mindscape.codecraftsample.utility.ScrollListener;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

public class ResturantViewModel extends ViewModel implements SwipeRefreshLayout.OnRefreshListener,

        OnSuccessListener<Location>, OnCompleteListener<LocationSettingsResponse>,

        ScrollListener {

    public static final String API_KEY = "AIzaSyDQ68qIEYv555MfNgxsVWVBGBLY8Rtg2Zc";

    private static final int LOCATION_SETTINGS_REQUEST = 0x1;
    private static final int REQUEST_LOCATION_PERMISSION_CODE = 2;
    private static final int LAST_LOCATION_PERMISSION_CODE = 3;
    private static ImageLoader imgLoader;
    public MutableLiveData<ResturantResponse.Result> selected;
    public ObservableBoolean loading;
    public Context context;
    public Activity activity;
    @Inject
    LocationRequest locationRequest;
    @Inject
    Task<LocationSettingsResponse> locationSettingsResponseTask;
    @Inject
    FusedLocationProviderClient fusedLocationProviderClient;
    private LocationCallback callback;
    private ResturantAdapter adapter;
    private GetResturantData getResturantData;

    @BindingAdapter({"bind:imageUrlList"})
    public static void loadImage(ImageView view, String imageUrl) {

        imgLoader.DisplayImage(imageUrl, R.mipmap.ic_launcher, view);

    }

    public void init(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        selected = new MutableLiveData<>();
        getResturantData = new GetResturantData(this);
        adapter = new ResturantAdapter(this);
        loading = new ObservableBoolean(true);
        imgLoader = new ImageLoader(context);
        ((CodeCraftApplication) context).getAppComponent().inject(this);
        locationSettingsResponseTask.addOnCompleteListener(this);
        callBack();
    }

    public void fetchList(Location location) {
        getResturantData.fetchList(location.getLatitude() + "," + location.getLongitude());
    }

    public MutableLiveData<List<ResturantResponse.Result>> getResults() {
        return getResturantData.getResults();
    }

    public MutableLiveData<ResturantResponse.Result> getSelected() {
        return selected;
    }

    public ResturantAdapter getAdapter() {
        return adapter;
    }

    public CustomScrollListener getListner() {
        return new CustomScrollListener(this);
    }

    public void setResultsInAdapter(List<ResturantResponse.Result> resultList) {
        this.adapter.setResultList(resultList);
        this.adapter.notifyDataSetChanged();
    }

    public void showToast(String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }


    public void onItemClick(Integer index) {
        selected.setValue(getResturantData.getResults().getValue().get(index));

    }


    public ResturantResponse.Result getResultData(Integer index) {
        if (getResturantData.getResults().getValue() != null &&
                index != null &&
                getResturantData.getResults().getValue().size() > index) {
            return (getResturantData.getResults().getValue().get(index));
        }
        return (null);
    }

    @Override
    public void onRefresh() {
        locationSettingsResponseTask.addOnCompleteListener(this);
        loading.set(false);

    }

    @Override
    public void onLoadMore() {
        if (getResturantData.next_page_token != null) {
            getResturantData.fetchAdditionalList(getResturantData.next_page_token);
        }

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == LOCATION_SETTINGS_REQUEST) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (AppUtils.checkPermissionActivity(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LAST_LOCATION_PERMISSION_CODE, activity)) {
                        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this);
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    AppUtils.showLocationSettingsAlert(activity);
                    loading.set(false);
                    break;
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LAST_LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this);
                } else {
                    AppUtils.showLocationPermissionAlert(activity);
                    loading.set(false);
                }

                break;
            case REQUEST_LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fusedLocationProviderClient.requestLocationUpdates(locationRequest, callback, Looper.myLooper());

                } else {
                    AppUtils.showLocationPermissionAlert(activity);
                    loading.set(false);
                }
                break;
        }
    }

    public void callBack() {
        callback = new LocationCallback() {
            @Override
            public void onLocationResult(final LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult != null) {
                    Location location = locationResult.getLastLocation();

                    fusedLocationProviderClient.removeLocationUpdates(callback);

                    if (AppUtils.isOnline(context)) {
//                        callApi(location);

                        fetchList(location);
                    } else {
                        AppUtils.showInterntOnlySettingsAlert(activity);
                        loading.set(false);
                    }


                }
            }
        };
    }

    @Override
    public void onSuccess(Location location) {
        if (location != null) {

            if (AppUtils.isOnline(context)) {
//                callApi(location);

                fetchList(location);
            } else {
                AppUtils.showInterntOnlySettingsAlert(activity);
                loading.set(false);

            }

        } else {
            if (AppUtils.checkPermissionActivity(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE, activity)) {
                fusedLocationProviderClient.requestLocationUpdates(locationRequest, callback, Looper.myLooper());

            }


        }
    }

    @Override
    public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
        try {
            LocationSettingsResponse response = task.getResult(ApiException.class);

            if (AppUtils.checkPermissionActivity(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LAST_LOCATION_PERMISSION_CODE, activity)) {
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this);
            }

        } catch (ApiException e) {
            e.printStackTrace();
            switch (e.getStatusCode()) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        ResolvableApiException resolvableApiException = (ResolvableApiException) e;
                        resolvableApiException.startResolutionForResult(activity, LOCATION_SETTINGS_REQUEST);

                    } catch (IntentSender.SendIntentException ex) {

                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                /*Location settings are not satisfied. However, we have no way to fix
                            the settings so we won't show the dialog.*/
                    //   swipe_refresh_layout.setRefreshing(false);
                    break;
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.map) {
            Intent intent = new Intent(context, ResturantMapsActivity.class);

            if (adapter != null) {
                List<ResturantResponse.Result> resultList = adapter.getResultList();
                if (resultList == null || resultList.size() == 0) {
                    Toast.makeText(context, "No data to show", Toast.LENGTH_SHORT).show();
                } else {

                    Gson gson = new Gson();
                    Type type = new TypeToken<List<ResturantResponse.Result>>() {
                    }.getType();
                    String jsonData = gson.toJson(resultList, type);
                    intent.putExtra("data", jsonData);
                    activity.startActivity(intent);
                }
            } else {
                Toast.makeText(context, "No data to show", Toast.LENGTH_SHORT).show();
            }

            return true;
        }
        return false;
    }

    public class CustomScrollListener extends RecyclerView.OnScrollListener {
        ScrollListener scrollListener;
        int lastVisible, totalItem;
        int threshold = 5;

        private CustomScrollListener(ScrollListener scrollListener) {
            this.scrollListener = scrollListener;
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

            lastVisible = layoutManager.findLastVisibleItemPosition();
            totalItem = layoutManager.getItemCount();
            if (totalItem <= (lastVisible + threshold)) {


                scrollListener.onLoadMore();

            }
        }
    }
}
