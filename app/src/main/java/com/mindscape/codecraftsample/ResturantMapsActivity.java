package com.mindscape.codecraftsample;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.ResultReceiver;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mindscape.codecraftsample.model.ResturantResponse;
import com.mindscape.codecraftsample.utility.AppUtils;
import com.mindscape.codecraftsample.utility.FetchAddressIntentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

public class ResturantMapsActivity extends FragmentActivity implements OnMapReadyCallback,
        OnSuccessListener<Location>, OnCompleteListener<LocationSettingsResponse>, GoogleMap.OnMarkerClickListener {
    private static final int LOCATION_SETTINGS_REQUEST = 0x1;
    private static final int REQUEST_LOCATION_PERMISSION_CODE = 2;
    private static final int LAST_LOCATION_PERMISSION_CODE = 3;
    private static final String TAG = "GeoLocation";
    public static List<Marker> markerList = new ArrayList<>();
    @Inject
    LocationRequest locationRequest;
    @Inject
    Task<LocationSettingsResponse> locationSettingsResponseTask;
    @Inject
    FusedLocationProviderClient fusedLocationProviderClient;
    LocationCallback callback;
    List<ResturantResponse.Result> responseData;
    Location defaultLocation = new Location("1");
    private GoogleMap mMap;
    private AddressResultReceiver mResultReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resturant_maps);
        ((CodeCraftApplication) getApplication()).getAppComponent().inject(this);
        locationSettingsResponseTask.addOnCompleteListener(this);
        defaultLocation.setLatitude(-34);
        defaultLocation.setLongitude(151);
        mResultReceiver = new AddressResultReceiver(new Handler());
        callBack();
        String data = getIntent().getStringExtra("data");
        responseData = new Gson().fromJson(
                data, new TypeToken<List<ResturantResponse.Result>>() {
                }.getType()
        );
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setOnMarkerClickListener(this);

        // Add a marker in Sydney and move the camera

        mMap.addMarker(new MarkerOptions().position(new LatLng(defaultLocation.getLatitude(), defaultLocation.getLongitude())).title("Marker in Sydney"));
        moveMap();
        updateMap();
    }

    public void moveMap() {
        CameraPosition defaultCameraPosition = new CameraPosition.Builder()
                .target(new LatLng(defaultLocation.getLatitude(), defaultLocation.getLongitude())).zoom(18f).tilt(0).build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(defaultCameraPosition));

    }

    public void updateMap() {

        try {
            for (int i = 0; i < responseData.size(); i++) {
                Marker mark = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(responseData.get(i).getGeometry().getLocation().getLat(),
                                responseData.get(i).getGeometry().getLocation().getLng()))
                );
                mark.setTag(responseData.get(i));
                markerList.add(mark);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
        try {
            LocationSettingsResponse response = task.getResult(ApiException.class);

            if (AppUtils.checkPermissionActivity(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LAST_LOCATION_PERMISSION_CODE, this)) {
                fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this);
            }

        } catch (ApiException e) {
            e.printStackTrace();
            switch (e.getStatusCode()) {
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    try {
                        ResolvableApiException resolvableApiException = (ResolvableApiException) e;
                        resolvableApiException.startResolutionForResult(this, LOCATION_SETTINGS_REQUEST);

                    } catch (IntentSender.SendIntentException ex) {

                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                /*Location settings are not satisfied. However, we have no way to fix
                            the settings so we won't show the dialog.*/
                    //   swipe_refresh_layout.setRefreshing(false);
                    break;
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Override
    public void onSuccess(Location location) {
        if (location != null) {

            defaultLocation = location;
            moveMap();
        } else {
            if (AppUtils.checkPermissionActivity(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION_PERMISSION_CODE, this)) {
                fusedLocationProviderClient.requestLocationUpdates(locationRequest, callback, Looper.myLooper());

            }


        }
    }

    public void callBack() {
        callback = new LocationCallback() {
            @Override
            public void onLocationResult(final LocationResult locationResult) {
                super.onLocationResult(locationResult);
                if (locationResult != null) {
                    Location location = locationResult.getLastLocation();

                    fusedLocationProviderClient.removeLocationUpdates(callback);
                    moveMap();
                    defaultLocation = location;


                }
            }
        };
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LAST_LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this);
                } else {
                    AppUtils.showLocationPermissionAlert(this);
                }

                break;
            case REQUEST_LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fusedLocationProviderClient.requestLocationUpdates(locationRequest, callback, Looper.myLooper());

                } else {
                    AppUtils.showLocationPermissionAlert(this);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check for the integer request code originally supplied to startResolutionForResult().
        if (requestCode == LOCATION_SETTINGS_REQUEST) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (AppUtils.checkPermissionActivity(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, LAST_LOCATION_PERMISSION_CODE, this)) {
                        fusedLocationProviderClient.getLastLocation().addOnSuccessListener(this);
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    AppUtils.showLocationSettingsAlert(this);
                    break;
            }
        }

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        ResturantResponse.Result tag = (ResturantResponse.Result) marker.getTag();

        startIntentService(defaultLocation, tag.getVicinity());
        return false;
    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService(Location mLocation, String destination) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(AppUtils.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(AppUtils.LOCATION_DATA_EXTRA, mLocation);
        intent.putExtra(AppUtils.LOCATION_DATA_EXTRA_DESTINATION, destination);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }

    private void showMapFromLocation(String src, String dest) {
        double srcLat = 0, srcLng = 0, destLat = 0, destLng = 0;
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            if (AppUtils.isOnline(this)) {
                List<Address> srcAddresses = geocoder.getFromLocationName(src,
                        1);
                if (srcAddresses.size() > 0) {
                    Address location = srcAddresses.get(0);
                    srcLat = location.getLatitude();
                    srcLng = location.getLongitude();
                }
                List<Address> destAddresses = geocoder.getFromLocationName(
                        dest, 1);
                if (destAddresses.size() > 0) {
                    Address location = destAddresses.get(0);
                    destLat = location.getLatitude();
                    destLng = location.getLongitude();
                }
                String desLocation = "&daddr=" + Double.toString(destLat) + ","
                        + Double.toString(destLng);
                String currLocation = "saddr=" + Double.toString(srcLat) + ","
                        + Double.toString(srcLng);
                // "d" means driving car, "w" means walking "r" means by bus
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?" + currLocation
                                + desLocation + "&dirflg=d"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                        & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                intent.setClassName("com.google.android.apps.maps",
                        "com.google.android.maps.MapsActivity");
                startActivity(intent);
            }
        } catch (Exception e) {
            Log.e(TAG, "Error when showing google map directions, E: " + e);
        }
    }

    /**
     * Receiver for data sent from FetchAddressIntentService.
     */
    private class AddressResultReceiver extends ResultReceiver {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from FetchAddressIntentService and updates the UI in HomeActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Show a toast message if an address was found.
            if (resultCode == AppUtils.SUCCESS_RESULT) {
                // Display the address string or an error message sent from the intent service.
                String mAddressOutput = resultData.getString(AppUtils.RESULT_DATA_KEY);
                String destination = resultData.getString(AppUtils.LOCATION_DATA_EXTRA_DESTINATION);

                if (mAddressOutput != null) {
                    showMapFromLocation(mAddressOutput, destination);
                }
            }
        }
    }
}
