package com.mindscape.codecraftsample.model;

import android.os.AsyncTask;
import android.util.Log;

import androidx.databinding.BaseObservable;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.mindscape.codecraftsample.viewmodel.ResturantViewModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import static com.mindscape.codecraftsample.viewmodel.ResturantViewModel.API_KEY;

public class GetResturantData extends BaseObservable {
    public String next_page_token;
    private ResturantViewModel resturantViewModel;
    private boolean loadMoreExecute = true;
    private MutableLiveData<List<ResturantResponse.Result>> resultList = new MutableLiveData<>();

    public GetResturantData(ResturantViewModel resturantViewModel) {
        this.resturantViewModel = resturantViewModel;
    }

    public GetResturantData() {
    }

    public MutableLiveData<List<ResturantResponse.Result>> getResults() {
        return resultList;
    }

    public void fetchList(String location) {

        loadMoreExecute = true;
        new AsyncTaskRunner().execute(location);
    }

    public void fetchAdditionalList(String next_page_token) {


        if (loadMoreExecute) {
            new AsyncTaskAdditionalData().execute(next_page_token);
        }
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpURLConnection httpConn = null;

                // Read text input stream.
                InputStreamReader isReader = null;

                // Read text into buffer.
                BufferedReader bufReader = null;

                // Save server response text.
                StringBuffer readTextBuf = new StringBuffer();
                URL url = new URL("https://maps.googleapis.com/maps/api/place/nearbysearch/json?type=restaurant&key=" + API_KEY + "&rankby=distance&location=" + params[0]);
                httpConn = (HttpURLConnection) url.openConnection();
                httpConn.setRequestMethod("GET");
                httpConn.setConnectTimeout(10000);
                httpConn.setReadTimeout(10000);
                InputStream inputStream = httpConn.getInputStream();
                isReader = new InputStreamReader(inputStream);
                bufReader = new BufferedReader(isReader);
                String line = bufReader.readLine();

                // Loop while return line is not null.
                while (line != null) {
                    // Append the text to string buffer.
                    readTextBuf.append(line);

                    // Continue to read text line.
                    line = bufReader.readLine();
                }
                try {
                    bufReader.close();
                    bufReader = null;

                    isReader.close();
                    isReader = null;

                    httpConn.disconnect();
                    httpConn = null;
                } catch (IOException ignored) {

                }
                Log.d("Json Response", readTextBuf.toString());
                return readTextBuf.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            Gson g = new Gson();
            ResturantResponse resturantResponse = g.fromJson(result, ResturantResponse.class);
            if (resturantResponse.getStatus().equals("OK")) {
                List<ResturantResponse.Result> results = resturantResponse.getResults();
                ResturantResponse.Result result1 = new ResturantResponse.Result(true);
                results.add(result1);
                resultList.setValue(results);
                next_page_token = resturantResponse.getNextPageToken();
            } else {
                resturantViewModel.loading.set(false);
                resturantViewModel.showToast("Exceeded quote limit");
            }

        }
    }

    private class AsyncTaskAdditionalData extends AsyncTask<String, String, String> {

        public AsyncTaskAdditionalData() {
            loadMoreExecute = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                HttpURLConnection httpConn = null;

                // Read text input stream.
                InputStreamReader isReader = null;

                // Read text into buffer.
                BufferedReader bufReader = null;

                // Save server response text.
                StringBuffer readTextBuf = new StringBuffer();
                URL url = new URL("https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=" + params[0] + "&key=" + API_KEY);
                httpConn = (HttpURLConnection) url.openConnection();
                httpConn.setRequestMethod("GET");
                httpConn.setConnectTimeout(10000);
                httpConn.setReadTimeout(10000);
                InputStream inputStream = httpConn.getInputStream();
                isReader = new InputStreamReader(inputStream);
                bufReader = new BufferedReader(isReader);
                String line = bufReader.readLine();

                // Loop while return line is not null.
                while (line != null) {
                    // Append the text to string buffer.
                    readTextBuf.append(line);

                    // Continue to read text line.
                    line = bufReader.readLine();
                }
                try {
                    bufReader.close();
                    bufReader = null;

                    isReader.close();
                    isReader = null;

                    httpConn.disconnect();
                    httpConn = null;
                } catch (IOException ignored) {

                }

                Log.d("Json Response", readTextBuf.toString());
                Log.d("Json Request", url.toString());

                return readTextBuf.toString();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String result) {
            Gson g = new Gson();
            ResturantResponse resturantResponse = g.fromJson(result, ResturantResponse.class);
            if (resturantResponse.getStatus().equals("OK")) {
                next_page_token = resturantResponse.getNextPageToken();
                int itemCount = resultList.getValue().size();
                int size = resturantResponse.getResults().size();
                resultList.getValue().addAll(itemCount - 1, resturantResponse.getResults());
                resultList.setValue(resultList.getValue());
                if (next_page_token == null) {
                    ResturantResponse.Result result1 = resultList.getValue().get(resultList.getValue().size() - 1);
                    result1.setName("End Of list");
                    resultList.setValue(resultList.getValue());
                }
                loadMoreExecute = true;
            } else {
                ResturantResponse.Result result1 = resultList.getValue().get(resultList.getValue().size() - 1);
                result1.setName("End Of list");
                resultList.setValue(resultList.getValue());
            }

        }


        @Override
        protected void onPreExecute() {

        }

    }
}
