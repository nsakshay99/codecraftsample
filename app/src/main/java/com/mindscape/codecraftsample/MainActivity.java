package com.mindscape.codecraftsample;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mindscape.codecraftsample.databinding.ActivityMainBinding;
import com.mindscape.codecraftsample.model.ResturantResponse;
import com.mindscape.codecraftsample.viewmodel.ResturantViewModel;

import java.lang.reflect.Type;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ResturantViewModel viewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupBindings(savedInstanceState);

    }

    private void setupBindings(Bundle savedInstanceState) {
        ActivityMainBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        viewModel = ViewModelProviders.of(this).get(ResturantViewModel.class);
        if (savedInstanceState == null) {
            viewModel.init(getApplicationContext(), this);
        }
        viewDataBinding.setModel(viewModel);
        setupListUpdate();

    }

    private void setupListUpdate() {
        viewModel.loading.set(true);
        viewModel.getResults().observe(this, new Observer<List<ResturantResponse.Result>>() {
            @Override
            public void onChanged(List<ResturantResponse.Result> results) {
                viewModel.loading.set(false);
                if (results.size() == 0) {
                    viewModel.loading.set(false);
                } else {
                    viewModel.setResultsInAdapter(results);
                    viewModel.loading.set(false);
                }
            }
        });
        viewModel.getSelected().observe(this, new Observer<ResturantResponse.Result>() {
            @Override
            public void onChanged(ResturantResponse.Result result) {
                Intent intent = new Intent(MainActivity.this, ResturantDetailActivity.class);
                Gson gson = new Gson();
                Type type = new TypeToken<ResturantResponse.Result>() {
                }.getType();
                String jsonData = gson.toJson(result, type);
                intent.putExtra("data", jsonData);
                startActivity(intent);
            }
        });

    }


    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check for the integer request code originally supplied to startResolutionForResult().
        viewModel.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.maps_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        viewModel.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

}
