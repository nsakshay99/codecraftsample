package com.mindscape.codecraftsample.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.mindscape.codecraftsample.BR;
import com.mindscape.codecraftsample.R;
import com.mindscape.codecraftsample.model.ResturantResponse;
import com.mindscape.codecraftsample.viewmodel.ResturantViewModel;

import java.util.List;

import static com.mindscape.codecraftsample.viewmodel.ResturantViewModel.API_KEY;

/**
 * Created by Andi on 3/28/2018.
 */

public class ResturantAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResturantResponse.Result> resultList;
    private ResturantViewModel viewModel;


    public ResturantAdapter(ResturantViewModel viewModel) {
        this.viewModel = viewModel;
    }

    private int getLayoutIdForPosition(int position) {
        return resultList.get(position).isLoad() ? R.layout.progress_item : R.layout.resturant_item;

    }

    public List<ResturantResponse.Result> getResultList() {
        return resultList;
    }

    public void setResultList(List<ResturantResponse.Result> resultList) {
        this.resultList = resultList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);
        if (viewType == R.layout.resturant_item) {
            return new NormalViewHolder(binding);
        } else {
            return new ProgressViewHolder(binding);
        }


    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, int position) {
        int itemViewType = viewHolder.getItemViewType();
        if (itemViewType == R.layout.resturant_item) {
            NormalViewHolder holder = (NormalViewHolder) viewHolder;
            holder.bind(viewModel, position);
        } else {
            ProgressViewHolder holder = (ProgressViewHolder) viewHolder;
            holder.bind(viewModel, position);

        }


    }

    @Override
    public int getItemCount() {
        return resultList == null ? 0 : resultList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return getLayoutIdForPosition(position);

    }

    class NormalViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding itemView;


        NormalViewHolder(ViewDataBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;


        }

        void bind(ResturantViewModel viewModel, Integer position) {
            String data = null;
            try {
                data = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=800&photoreference=" + viewModel.getResultData(position).getPhotos().get(0).getPhotoReference() + "&key=" + API_KEY;
            } catch (Exception e) {
                e.printStackTrace();
            }

            itemView.setVariable(BR.imageUrl, data);
            itemView.setVariable(BR.viewModel, viewModel);
            itemView.setVariable(BR.position, position);
            itemView.executePendingBindings();
        }
    }

    class ProgressViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding itemView;


        ProgressViewHolder(ViewDataBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }

        void bind(ResturantViewModel viewModel, Integer position) {
            itemView.setVariable(BR.viewModel, viewModel);
            itemView.setVariable(BR.position, position);
            itemView.executePendingBindings();
        }
    }
}

