package com.mindscape.codecraftsample;

import android.app.Application;
import android.content.Context;

import com.mindscape.codecraftsample.Dagger.AppComponent;
import com.mindscape.codecraftsample.Dagger.AppModule;
import com.mindscape.codecraftsample.Dagger.DaggerAppComponent;

/**
 * Created by ${Akshay} on 15-10-2019.
 */

public class CodeCraftApplication extends Application {
    private AppComponent appComponent;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);


    }


    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = initDagger(this);

    }


    public AppComponent getAppComponent() {
        return appComponent;
    }

    protected AppComponent initDagger(CodeCraftApplication application) {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(application))
                .build();
    }
}
