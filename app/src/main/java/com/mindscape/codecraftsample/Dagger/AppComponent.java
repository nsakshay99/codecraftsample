package com.mindscape.codecraftsample.Dagger;

import com.mindscape.codecraftsample.ResturantMapsActivity;
import com.mindscape.codecraftsample.viewmodel.ResturantDetailViewModel;
import com.mindscape.codecraftsample.viewmodel.ResturantViewModel;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by ${Akshay} on 15-10-2019.
 */

@Singleton
@Component(modules = {AppModule.class, GoogleLocationApi.class})
public interface AppComponent {


    void inject(ResturantDetailViewModel mainActivity);

    void inject(ResturantMapsActivity resturantMapsActivity);

    void inject(ResturantViewModel resturantMapsActivity);


}
