package com.mindscape.codecraftsample.Dagger;

import android.content.Context;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.Task;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ${Akshay} on 15-10-2019.
 */
@Module
public class GoogleLocationApi {

    @Provides
    @Singleton
    LocationRequest providesLocationRequest() {
        /*
         * The desired interval for location updates. Inexact. Updates may be more or less frequent.
         */
        final long UPDATE_INTERVAL_IN_MILLISECONDS = 2000;
        /*
          The fastest rate for active location updates. Exact. Updates will never be more frequent
          than this value.
         */
        final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

        LocationRequest mLocationRequest = LocationRequest.create();
        // Sets the desired interval for active location updates. This interval is
        // inexact. You may not receive arerawhatsapp@2017 updates at all if no location sources are available, or 7024
        // you may receive them slower than requested. You may also receive updates faster than
        // requested if other applications are requesting location at a faster interval.
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Provides
    @Singleton
    Task<LocationSettingsResponse> providesLocationSettingsRequest(LocationRequest locationRequest, Context context) {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);
        return LocationServices.getSettingsClient(context).checkLocationSettings(builder.build());
    }

    @Provides
    @Singleton
    FusedLocationProviderClient providesFusedLocationProviderClient(Context context) {
        return LocationServices.getFusedLocationProviderClient(context);

    }
}
