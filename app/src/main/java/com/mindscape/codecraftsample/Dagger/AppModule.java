package com.mindscape.codecraftsample.Dagger;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ${Akshay} on 15-10-2019.
 */

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }


    @Provides
    @Singleton
    Context provideContext() {
        return application;

    }

}
