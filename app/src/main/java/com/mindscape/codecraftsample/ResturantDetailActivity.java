package com.mindscape.codecraftsample;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.mindscape.codecraftsample.databinding.ActivityResturantDetailBinding;
import com.mindscape.codecraftsample.viewmodel.ResturantDetailViewModel;

public class ResturantDetailActivity extends AppCompatActivity {


    private ResturantDetailViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resturant_detail);
        setupBindings(savedInstanceState);
    }


    private void setupBindings(Bundle savedInstanceState) {
        ActivityResturantDetailBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_resturant_detail);
        viewModel = ViewModelProviders.of(this).get(ResturantDetailViewModel.class);
        if (savedInstanceState == null) {
            viewModel.init(getApplicationContext(), this, getIntent().getStringExtra("data"));
        }
        viewDataBinding.setModel(viewModel);
//        setupImageLoad();

    }

    private void setupImageLoad() {
        viewModel.setData(getIntent().getStringExtra("data"));
    }


}
